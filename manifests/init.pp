class uc3_init {
  $dir = "/${id}/init.d"
  
  file { $dir :
    ensure => directory;
  }
}

define uc3_init::script(
  $pid_file,
  $restart = undef,
  $setup   = undef,
  $start,
  $status  = undef,
  $stop) {
    file { "${uc3_init::dir}/$title" :
      mode    => 755,
      content => template("uc3_init/init_script.erb") ;
    }
  }

